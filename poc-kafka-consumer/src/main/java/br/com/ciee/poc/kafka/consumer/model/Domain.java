/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.poc.kafka.consumer.model;

import lombok.Data;

/**
 * Classe que representa os domínios da aplicação.
 * @author vitor
 */
@Data
public class Domain {
    
    private int idDomain;
    private int idParentDomain;
}