/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.poc.kafka.consumer.model;

import java.util.List;
import lombok.Data;

/**
 * Classe que representa a associação entre usuários e domínios.
 * @author vitor
 */
@Data
public class UserDomain {
    
    private int idUser;
    private List<Domain> domains;
}
