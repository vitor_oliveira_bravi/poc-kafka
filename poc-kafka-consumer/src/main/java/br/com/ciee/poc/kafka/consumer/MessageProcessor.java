/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.poc.kafka.consumer;

/**
 * Contrato que define o padrão de processamento de uma mensagem vinda de um
 * tópico do Kafka
 * @param <T> O tipo do objeto esperado no recebimento.
 * @author vitor
 */
public interface MessageProcessor<T> {
    /**
     * Realiza o processamento do json recebido pelo Kafka após a sua conversão
     * em objeto.
     * @param data O objeto preenchido com os dados obtidos pelo json do tópico.
     */
    void process(T data);
}