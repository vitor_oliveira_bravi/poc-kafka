/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.poc.kafka.consumer;

import br.com.ciee.poc.kafka.consumer.model.UserDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Implementação de exemplo para o processamento da mensagem do Kafka.
 * @author vitor
 */
@Component
public class ExampleProcessor implements MessageProcessor<UserDomain> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExampleProcessor.class);
    
    @Override
    public void process(UserDomain data) {
        LOGGER.info("Successfull received: {}", data);
    }
}
