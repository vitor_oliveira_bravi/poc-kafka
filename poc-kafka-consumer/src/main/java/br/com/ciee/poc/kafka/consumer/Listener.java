/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.poc.kafka.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

/**
 * Classe responsável por consumir as mensagens do tópico declarado.
 * @author vitor
 */
@Service
public class Listener {
    private static final Logger LOGGER = LoggerFactory.getLogger(Listener.class);
    
    @Autowired 
    private MessageProcessor processor;
    
    @Value("${topic.expected-type}")
    private String expectedType;
    
    @PostConstruct
    public void init() {
        LOGGER.info("Listener started");
    }
    /**
     * Método que recebe a mensagem do tópico configurado através da propriedade 
     * ${topic.name}, realiza a conversão do json recebido para a entidade
     * configurada pela propriedade ${topic.expected-type} e invoca a implementação
     * de {@link MessageProcessor}
     * @param message O json obtido do tópico
     * @param headers Os headers da mensagem, se existirem.
     * @throws ClassNotFoundException Caso a classe esperada informada na propriedade
     * não exista no classpath.
     */
    @KafkaListener(topics = "${topic.name}")
    public void receive(@Payload String message, @Headers MessageHeaders headers) throws ClassNotFoundException {
        LOGGER.info("Received message: {}", message);
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object object = mapper.readValue(message, Class.forName(expectedType));
            processor.process(object);
        } catch (IOException ex) {
            LOGGER.error("Error to convert message: {}", ex);
        }
    }
}