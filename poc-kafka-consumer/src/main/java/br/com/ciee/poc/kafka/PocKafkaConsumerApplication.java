package br.com.ciee.poc.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocKafkaConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocKafkaConsumerApplication.class, args);
    }
}
