# poc-kafka
Esta prova de conceito tem como objetivo oferecer um modelo para implementação de envio e recebimento de mensagens via Apacha Kafka.

# Organização
O projeto está organizado em dois projetos **producer** e **consumer** representando o envio e a leitura de uma mensagem do tópico respectivamente.

# poc-kafka-producer
O projeto producer contém a classe no modelo da entidade que temos hoje com campos reduzidos para teste e uma classe responsável pela conversão desses dados em formato json e envio para o tópico do kafka. A classe Sender recebe um objeto qualquer como parâmetro, converte para o formato json e realiza o envio para o tópico

### Configuração
O arquivo **application.yml** contém as configurações para execução do kafka tais como apontamento para o servidor kafka configurado por variáveis de ambiente ou localhost:9092 como default. Além disso, nas configurações de tópico existe uma propriedade **topic.name** que deve conter o nome do tópico que será consumido no kafka (o mesmo nome presente no docker-compose).
O arquivo **docker-compose.yml** apresenta a configuração para execução do kafka em ambiente docker. Importante atentar-se as propriedades **KAFKA_ADVERTISED_HOST_NAME** (que deverá conter o IP do servidor onde será executado o kafka ou 127.0.0.1 para execução localhost) e **KAFKA_CREATE_TOPICS** (que deverá conter a lista de tópicos que serão criados na subida do servidor kafka). 

# poc-kafka-consumer
O projeto consumer contém a classe que faz a leitura das informações do tópico. Esta classe realiza a conversão do json de entrada para o tipo especificado que deve estar configurado nas propriedades da aplicação e depois invoca uma implementação da interface **MessageProcessor** para trabalhar com o objeto já preenchido.

### Configuração
O arquivo **application.yml** contém as configurações para execução do kafka tais como apontamento para o servidor kafka configurado por variáveis de ambiente ou localhost:9092 como default. Além disso, nas configurações de tópico existe uma propriedade **topic.name** que deve conter o nome do tópico que será consumido no kafka (o mesmo nome presente no docker-compose) e a propriedade **expected-type** que deve conter o nome totalmente qualificado da classe que será utilizada para mapear o json que foi consumido do tópico.

# Execução
Primeiramente deverá ser criado o ambiente kafka através do comando **docker-compose up -d** na mesma pasta onde o arquivo do docker-compose estiver presente. Depois deverá ser executado o projeto **poc-kafka-consumer** e depois o projeto **poc-kafka-producer** que jogará uma mensagem no tópico. Feito isso, basta acompanhar os logs para acompanhar a mensagem saindo do producer e chegando no consumer. 
