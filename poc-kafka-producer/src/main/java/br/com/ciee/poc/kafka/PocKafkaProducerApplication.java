package br.com.ciee.poc.kafka;

import br.com.ciee.poc.kafka.producer.Domain;
import br.com.ciee.poc.kafka.producer.Sender;
import br.com.ciee.poc.kafka.producer.UserDomain;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocKafkaProducerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PocKafkaProducerApplication.class, args);
    }
    
    @Autowired
    private Sender sender;

    @Override
    public void run(String... args) throws Exception {
        UserDomain userDomains = new UserDomain();
        userDomains.setIdUser(123);
        userDomains.setDomains(Arrays.asList(new Domain(1, 2), new Domain(2, 3), new Domain(4, 6), new Domain(7, 2)));
        
        sender.send(userDomains);
        Thread.sleep(30000);
    }
}
