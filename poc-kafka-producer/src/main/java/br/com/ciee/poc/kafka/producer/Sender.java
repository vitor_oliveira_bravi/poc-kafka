/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.poc.kafka.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * Classe responsável por realizar o envio de uma mensagem para um tópico.
 * @author vitor
 */
@Service
public class Sender {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);
    
    @Value("${topic.name}")
    private String topic;
    
    @Autowired
    private KafkaTemplate<String, String> template;
    
    /**
     * Método responsável por enviar a mensagem para um determinado tópico
     * @param payload O objeto que será convertido em json e enviado para o tópico.
     * @throws JsonProcessingException Caso ocorra algum erro durante a conversão
     * do payload em json
     */
    public void send(Object payload) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(payload);
        
        template.send(topic, json);
        LOGGER.info("sending payload='{}' to topic='{}'", json, topic);
    }
}