/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ciee.poc.kafka.producer;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Classe que representa os domínios da aplicação.
 * @author vitor
 */
@Data
@NoArgsConstructor
public class Domain {
    
    private int idDomain;
    private int idParentDomain;

    public Domain(int idDomain, int idParentDomain) {
        this.idDomain = idDomain;
        this.idParentDomain = idParentDomain;
    }
}